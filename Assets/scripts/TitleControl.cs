﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleControl : MonoBehaviour {

    public bool sway = false;
    bool triggered = false;

    void Start() {
        
    }
    
    void Update() {
        if (Input.anyKey && !triggered && Time.time > 1.0f) {
            triggered = true;
            GetComponent<ConstantForce>().force = new Vector3(0, 5.0f, 0);
            StartCoroutine(DelayDestuct());
        }

        if (sway) {
            Vector3 rot = transform.eulerAngles;
            rot.y = rot.y + Mathf.Cos(Time.time/1.5f)/(2*Mathf.PI*7);
            transform.eulerAngles = new Vector3(rot.x, rot.y, rot.z);
        }
    }

    IEnumerator DelayDestuct() {
        yield return new WaitForSeconds(2);
        GameObject.Destroy(gameObject);
    }

}
