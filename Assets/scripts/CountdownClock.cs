﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class CountdownClock : MonoBehaviour {

	float timer = 13f;
	bool countdownStarted;

    void Start() {
        transform.GetChild(0).GetComponent<TextMeshPro>().text = "00:" + Mathf.CeilToInt(timer).ToString("00");
    }

    void Update() {
		if(countdownStarted) {
			if(timer > 0)
				timer -= Time.deltaTime;

			transform.GetChild(0).GetComponent<TextMeshPro>().text = "00:" + Mathf.CeilToInt(timer).ToString("00");
		}
    }

    public void startCountdown() {
		countdownStarted = true;
    }
}
