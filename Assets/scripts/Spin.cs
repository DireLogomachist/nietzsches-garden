﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour {

    public float spin_speed = 25;
    public bool spinning = true;
    
    void Start() {
        
    }
    
    void Update() {
        if(spinning) transform.Rotate(new Vector3(0, 1, 0), spin_speed*Time.deltaTime);
    }
}
