﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class butterfly : MonoBehaviour {

    GameObject left;
    GameObject right;

    void Start() {
        left = transform.GetChild(0).gameObject;
        right = transform.GetChild(1).gameObject;
    }
    
    void Update() {
        float rot = Mathf.Sin(Time.time*8)*60;
        left.transform.localEulerAngles = new Vector3(0, 0, rot);
        right.transform.localEulerAngles = new Vector3(0, 0, -rot);
    }
}
