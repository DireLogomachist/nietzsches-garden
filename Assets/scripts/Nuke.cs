﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Nuke : MonoBehaviour {

    float rise_speed = 15;
    float dive_accel = 5;
    bool triggered = false;
    bool planted = false;
    Vector3 dive_target = new Vector3(60, 0, 26);

    Vector3 prev_pos;
    
    AudioSource boop;
    AudioClip bing_clip;
    AudioClip detonation_clip;
    AudioSource boom;

    public UnityEvent approach;
    public UnityEvent impact;
    public UnityEvent detonate;

    public GameObject[] textMeshes;

    void Start() {
        boop = transform.GetComponentsInChildren<AudioSource>()[0];
        boom = transform.GetComponentsInChildren<AudioSource>()[1];
        boom.clip = (AudioClip) Resources.Load("audio/missile_impact");
    	boop.clip = (AudioClip) Resources.Load("audio/beep2");
        bing_clip = (AudioClip) Resources.Load("audio/beep2");
        detonation_clip = (AudioClip) Resources.Load("audio/detonation");

        prev_pos = transform.position;

        foreach(GameObject g in textMeshes) {
            g.GetComponent<MeshRenderer>().enabled = false;
        }
    }

    void Update() {
        if (!triggered && Input.anyKey && Time.time > 1.0f) {
            triggered = true;
            GetComponent<Animator>().enabled = true;
        }
        
        if(transform.position.y < -2 && !planted) {
            GetComponent<Animator>().enabled = false;
            GetComponentInChildren<Spin>().spinning = false;
            transform.position = new Vector3(62.4f, 6.55f, 27.2f);
            transform.eulerAngles = new Vector3(59.4f, -120.349f, 0);
            transform.GetComponentInChildren<ParticleSystem>().Stop();
            
            planted = true;
            impact.Invoke();
            boom.Play();
            StartCoroutine(countdownTrigger());
        }

        if(!planted) {
            if(((transform.position-prev_pos)) != Vector3.zero) {
                Quaternion rotation = Quaternion.LookRotation(((transform.position-prev_pos)).normalized);
                transform.rotation = rotation;
            }
            prev_pos = transform.position;
        }
    }

    IEnumerator countdownBeep() {
        for(int i = 0; i < 9; i++) {
            yield return new WaitForSeconds(1);
            boop.Play();
        }
        boop.clip = bing_clip;
        detonate.Invoke();
        yield return new WaitForSeconds(0.8f);
        boom.volume = 1.0f;
        boom.PlayOneShot(detonation_clip);
    }

    IEnumerator countdownTrigger() {
        yield return new WaitForSeconds(3);
        foreach(GameObject g in textMeshes) {
            g.GetComponent<MeshRenderer>().enabled = true;
        }
        StartCoroutine(countdownBeep());
    }
    
    void approachTrigger() {
        approach.Invoke();
    }
}
