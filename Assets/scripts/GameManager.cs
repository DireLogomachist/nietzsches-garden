﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	private static GameManager _inst;
	public static GameManager inst { get { return _inst; } }

	public bool playMusic = false;
	public bool showFPS = false;
	AudioSource backgroundMusic;

	bool missile_hit = false;
	bool detonation = false;

	GameObject rubble;
	GameObject detonationFlare;

	void Start() {
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;

		backgroundMusic = gameObject.AddComponent<AudioSource>();
		backgroundMusic.clip = (AudioClip) Resources.Load("audio/Komiku - Poupi';s incredible adventures ! - Ending");
		backgroundMusic.volume = 0.4f;
		if (playMusic)
			backgroundMusic.Play();

		rubble = GameObject.Find("Rubble");
		rubble.SetActive(false);
		detonationFlare = GameObject.Find("Nuke Flare");
	}

	void Update () {
	}

	void OnGUI() {
		if (showFPS)
			GUI.Label(new Rect(50,20,500,500), "FPS: " + Mathf.Round((1.0f/Time.smoothDeltaTime)).ToString());
	}

	public void detonate() {
		StartCoroutine(detonateCoroutine());
	}

	IEnumerator detonateCoroutine() {
		gameObject.transform.position = new Vector3(60.0f, 60.0f, -118.5f);
		gameObject.transform.GetChild(0).transform.eulerAngles = new Vector3(0,0,0);
		gameObject.transform.eulerAngles = new Vector3(11.85f, 0.0f, 0.0f);
		RenderSettings.fogDensity = 0.002f;
		yield return new WaitForSeconds(1);
		GameObject.Find("Garden").transform.position -= new Vector3(0, 150, 0);
		GameObject.Find("nuke").transform.position -= new Vector3(0, 150, 0);
		rubble.SetActive(true);
		detonationFlare.GetComponent<Light>().enabled = true;
		GameObject.Find("Mushroom Cloud").GetComponent<ParticleSystem>().Play();
		GameObject.Find("Mushroom Cloud").GetComponent<Animator>().Play("Mushroom");
		GameObject.Find("Mushroom Base").GetComponent<ParticleSystem>().Play();
		GameObject.Find("Mushroom Base").GetComponent<Animator>().Play("MushroomBase");
		yield return new WaitForSeconds(7);
		GameObject.Find("Canvas").transform.GetComponentInChildren<Animator>().Play("FadeOut");
	}

	public void muteTrack() {
		StartCoroutine(muteTrackCoroutine());
	}

	IEnumerator muteTrackCoroutine() {
		float muteTime = Time.time;
		float maxVolume = backgroundMusic.volume;
		while((Time.time - muteTime) < 1.5f) {
			float vol = maxVolume*(1.0f - (Time.time - muteTime)/1.5f);
			backgroundMusic.volume =  vol;
			yield return null;
		}
		backgroundMusic.Stop();
	}
}
