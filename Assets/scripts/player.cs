﻿
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class player : MonoBehaviour {

	public float speed = 30.0F;
	public float gravity = 20.0F;
	public float shakeIndex = 0.0f;
	
	float y_origin;

	float maxRot = 0.4f;
	float maxOffset = 0.02f;
	float shakeFallspeed = 0.4f;

	Vector3 lastOffset;
	Vector3 lastRot;

	CharacterController controller;

	void Start () {
		controller = GetComponent<CharacterController>();
		y_origin = transform.position.y;
		lastOffset = new Vector3(0,0,0);
		lastRot = new Vector3(0,0,0);
	}
	
	void Update () {
		Vector3 forward = transform.TransformDirection(Camera.main.transform.forward);
		Vector3 right = transform.TransformDirection(Camera.main.transform.right);
		forward.y = 0;
		forward.Normalize();
		right.y = 0;
		right.Normalize();

        Vector3 move_dir = speed * Vector3.Normalize(forward*Input.GetAxis("Vertical")+right*Input.GetAxis("Horizontal"));
		move_dir.y -= gravity * Time.deltaTime;

		if(Time.time > 1.0f) controller.SimpleMove(move_dir);
		transform.position = new Vector3(transform.position.x, y_origin, transform.position.z);

		shakeUpdate();
	}

	public void disablePlayer(){
		GetComponent<CharacterController>().enabled = false;
		GetComponentInChildren<SmoothMouseLook>().enabled = false;
		enabled = false;
	}

	public void slowShake() {
		GetComponent<Animator>().Play("Shake");
	}

	void shakeUpdate() {

		transform.position = transform.position - lastOffset;
		transform.eulerAngles = transform.eulerAngles - lastRot;

		if(shakeIndex > 0.0f) {
			float yaw = maxRot*shakeIndex*shakeIndex*Random.Range(-1f,1f);
			float pitch = maxRot*shakeIndex*shakeIndex*Random.Range(-1f,1f);
			float roll = maxRot*shakeIndex*shakeIndex*Random.Range(-1f,1f);

			float offsetX = maxOffset*shakeIndex*shakeIndex*Random.Range(-1f,1f);
			float offsetY = maxOffset*shakeIndex*shakeIndex*Random.Range(-1f,1f);
			float offsetZ = maxOffset*shakeIndex*shakeIndex*Random.Range(-1f,1f);

			transform.position = transform.position + new Vector3(offsetX, offsetY, offsetZ);
			transform.eulerAngles = transform.eulerAngles + new Vector3(yaw, pitch, roll);

			shakeIndex = Mathf.Clamp(shakeIndex - shakeFallspeed*Time.deltaTime, 0.0f, Mathf.Infinity);

			lastOffset = new Vector3(offsetX,offsetY,offsetZ);
			lastRot = new Vector3(yaw,pitch,roll);
		} else {
			lastOffset = new Vector3(0,0,0);
			lastRot = new Vector3(0,0,0);
		}

	}
}
