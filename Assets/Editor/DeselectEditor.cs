﻿using UnityEngine;
using UnityEditor;

public class DeselectEditor : ScriptableObject {
    [MenuItem("GameObject/Deselect all %#D")]
    static void DeselectAll() {
        Selection.objects = new Object[0];
    }
}
