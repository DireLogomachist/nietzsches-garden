﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(RectSpawner))]
public class RectSpawnerEditor : Editor {

    GameObject prefab;
    
    Vector3[] coords = {new Vector3(1,0,1), new Vector3(1,0,-1), new Vector3(-1,0,-1), new Vector3(-1,0,1)};
    Vector3[] dirs = new Vector3[] {Vector3.forward, Vector3.right, -Vector3.forward, -Vector3.right};
    Vector3[] mask = {new Vector3(1,1,0), new Vector3(0,1,1), new Vector3(1,1,0), new Vector3(0,1,1)};

    void OnEnable() {
        RectSpawner rectSpawner = (RectSpawner) target;
        prefab = rectSpawner.prefab;
        SyncCoords();
    }

    void OnSceneGUI() {
        DrawGui();
        EditorInput();
    }
    
    void DrawGui() {
        RectSpawner rectSpawner = (RectSpawner) target;
        for(int i=0; i<4; i++) {
            Vector3 p1 = rectSpawner.transform.position + coords[i];
            Vector3 p2 = new Vector3();
            
            if(i != 3) p2 = rectSpawner.transform.position + coords[i+1];
            else p2 = rectSpawner.transform.position + coords[0];
            
            Handles.DrawLine(p1, p2);
        }
    }

    void EditorInput() {
        RectSpawner rectSpawner = (RectSpawner) target;
        for(int i=0; i<4; i++) {
            EditorGUI.BeginChangeCheck();
            Handles.color = Color.green;
            
            Vector3 newTargetPosition = Handles.Slider(rectSpawner.transform.position + dirs[i]*rectSpawner.nodePos[i], dirs[i], 0.2f, Handles.CubeHandleCap, .1f);
            if (EditorGUI.EndChangeCheck()) {
                rectSpawner.nodePos[i] = (newTargetPosition - rectSpawner.transform.position).magnitude;
                if (rectSpawner.nodePos[i] < 0) rectSpawner.nodePos[i] = 0;
                SyncCoords();
                SpawnGrass();
            }
        }
    }

    void SpawnGrass() {
        RectSpawner rectSpawner = (RectSpawner) target;
         foreach (Transform child in rectSpawner.transform) {
            GameObject.DestroyImmediate(child.gameObject);
        }

        for(int i = 0; i < rectSpawner.objCount; i++) {
            float x1 = rectSpawner.nodePos[1];
            float x2 = -rectSpawner.nodePos[3];
            float y1 = rectSpawner.nodePos[0];
            float y2 = -rectSpawner.nodePos[2];

            float x = Random.Range(x1, x2);
            float y = Random.Range(y1, y2);

            Vector3 eulerAngle = rectSpawner.transform.eulerAngles;
            eulerAngle.y = Random.Range(0f, 360f);

            GameObject g = Instantiate(prefab, rectSpawner.transform.position + new Vector3(x,0,y), rectSpawner.transform.rotation, rectSpawner.transform);
            g.transform.eulerAngles = eulerAngle;
        }
    }

    void SyncCoords() {
        RectSpawner rectSpawner = (RectSpawner) target;
        for(int i=0; i<4; i++) {
            if (i != 0) {
                coords[i] = Vector3.Scale(coords[i],mask[i]) + dirs[i]*rectSpawner.nodePos[i];
                coords[i-1] = Vector3.Scale(coords[i-1],mask[i]) + dirs[i]*rectSpawner.nodePos[i];
            } else {
                coords[i] = Vector3.Scale(coords[i],mask[i]) + dirs[i]*rectSpawner.nodePos[i];
                coords[3] = Vector3.Scale(coords[3],mask[i]) + dirs[i]*rectSpawner.nodePos[i];
            }
        }
    }

}
