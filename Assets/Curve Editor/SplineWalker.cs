﻿using UnityEngine;

public class SplineWalker : MonoBehaviour {

	public BezierSpline spline;

	public float duration;
	public bool lookForward;
	public SplineWalkerMode mode;

	public float progress;
	public bool goingForward = true;
	public bool walkerEnabled = true;

	private void Update () {
		if(walkerEnabled) {
			if (goingForward) {
				progress += Time.deltaTime / duration;
				if (progress > 1f) {
					if (mode == SplineWalkerMode.Once) {
						progress = 1f;
					}
					else if (mode == SplineWalkerMode.Loop) {
						progress -= 1f;
					}
					else {
						progress = 2f - progress;
						goingForward = false;
					}
				}
			}
			else {
				progress -= Time.deltaTime / duration;
				if (progress < 0f) {
					progress = -progress;
					goingForward = true;
				}
			}

			Vector3 position = spline.GetPoint(progress);
			if (progress != 1.0f) transform.localPosition = position;
			if (lookForward) {
				transform.LookAt(position + spline.GetDirection(progress));
			}
		}
	}
}